FROM debian:buster

RUN apt-get update \
    && apt-get install -y vim \  
    && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends ntp \
    && apt-get clean -q \
    && rm -Rf /var/lib/apt/lists/*

COPY ntp.conf /etc/ntp.conf
 
RUN chgrp root /var/lib/ntp && chmod g+w /var/lib/ntp

EXPOSE 123/udp

ENTRYPOINT ["/usr/sbin/ntpd"]
