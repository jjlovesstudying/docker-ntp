### Introduction
* Purpose: To run an offline ntp server in a docker container
* Adapted from the below references
* Uses local time in the container as the ntp time instead of "official" timeservers
* Uses debian:buster instead of ubuntu:16.04 for smaller image size
* Added vim in case editing of scripts is necessary

### To build image

```
docker build -t jjlovesstudying/ntp .
```

### To run container
```
docker run --cap-drop ALL --cap-add NET_BIND_SERVICE --cap-add SYS_TIME --cap-add SYS_RESOURCE \
           --name my_ntp \
           --restart always --detach \
           --expose 123/udp jjlovesstudying/ntp -g -n
```

### To query from network linux computer
```
ntpdate -q ip_address
```

#### References
https://github.com/jcberthon/containers/tree/armhf/ntp
https://www.berthon.eu/2017/a-time-server-in-a-container-part-1/

